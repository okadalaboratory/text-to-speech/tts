#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.
from gtts import gTTS
from playsound import playsound
def main():
    tts =gTTS(text="今日は朝から雨です",lang="ja")
    tts.save(r'/tmp/result.mp3')
   
    #　再生してみる
    playsound("/tmp/result.mp3")

if __name__ == "__main__":
    main()

