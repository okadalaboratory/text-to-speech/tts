# gTTS

品質：A<br>
オンライン<br>
日本語、英語<br>
MacBook Air（M2、2022）,macOS Monterey 12.6<br>
Ubuntu20.04<br>
音声合成のコアな部分は、Google翻訳音声合成APIに依存し、Google Cloud Text-to-Speechとは関係ない。

事前の設定もなく、無料で簡単に試せる

## インストール
gTTSのpythonモジュールをインストールします
```
pip3 install gTTS
```
「Successfully installed」と表示されれば正常にインストールが完了。

## 使い方
gTTSの使い方はとても簡単です。指定した文字列が音声ファイルとして保存されます。
```
$ python3
>>> from gtts import gTTS
>>> tts =gTTS(text="今日は朝から雨です",lang="ja")
>>> tts.save(r'result.mp3')

英語の場合はjaをusに変えてください。


