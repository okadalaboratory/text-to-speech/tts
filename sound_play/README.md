# sound_play
音声合成や音声・音楽ファイル、内蔵サウンドを再生するROSパッケージ

英語だけで日本語には対応していないので注意

オフラインで使えるので便利

## 動作を確認した環境
Ubuntu20.04, Noetic

## インストール
```
sudo apt-get install ros-noetic-sound-play
```

## ノードの起動
```
roslaunch sound_play soundplay_node.launch
```

## テストしてみる
```
 rosrun sound_play test.py
```

## 音声ファイルの再生 (WAV, OGG)
```
rosrun sound_play play.py --help
rosrun sound_play play.py /opt/ros/noetic/share/sound_play/sounds/say-beep.wav
rosrun sound_play play.py /opt/ros/noetic/share/sound_play/sounds/BACKINGUP.ogg
```

## 内蔵サウンドの再生
```
rosrun sound_play playbuiltin.py --help
rosrun sound_play playbuiltin.py 2
```

## 組み込み音声の一覧
```
cat `rospack find sound_play`/msg/SoundRequest.msg
```

## 音声合成（TTS）
```
rosrun sound_play say.py --help
rosrun sound_play say.py 'Good morning'
echo Hello again | rosrun sound_play say.py
```

## 参考
[ROS.org: sound_play](http://wiki.ros.org/ja/sound_play)<br>
[ROS.org: How to Configure and Use Speakers with sound_play](http://wiki.ros.org/sound_play/Tutorials/ConfiguringAndUsingSpeakers)
