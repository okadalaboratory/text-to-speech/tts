#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.
import rospy
import os, sys
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

def main():    
    rospy.init_node('soundplay_test', anonymous = True)
    soundhandle = SoundClient()
    rospy.sleep(1)

    soundhandle.stopAll()
    rospy.loginfo("This script will run continuously until you hit CTRL+C, testing various sound_node sound types.")

    #print 'Try to play wave files that do not exist.'
    #soundhandle.playWave('17')
    #soundhandle.playWave('dummy')

    # print 'say'
    # soundhandle.say('Hello world!')
    # sleep(3)

    rospy.loginfo('wave')
    soundhandle.playWave('say-beep.wav')
    rospy.sleep(2)

    rospy.loginfo('quiet wave')
    soundhandle.playWave('say-beep.wav', 0.3)
    rospy.sleep(2)

    rospy.loginfo('plugging')
    soundhandle.play(SoundRequest.NEEDS_PLUGGING)
    rospy.sleep(2)

    rospy.loginfo('quiet plugging')
    soundhandle.play(SoundRequest.NEEDS_PLUGGING, 0.3)
    rospy.sleep(2)

    rospy.loginfo('unplugging')
    soundhandle.play(SoundRequest.NEEDS_UNPLUGGING)
    rospy.sleep(2)

    rospy.loginfo('plugging badly')
    soundhandle.play(SoundRequest.NEEDS_PLUGGING_BADLY)
    rospy.sleep(2)

    rospy.loginfo('unplugging badly')
    soundhandle.play(SoundRequest.NEEDS_UNPLUGGING_BADLY)
    rospy.sleep(2)

    s1 = soundhandle.builtinSound(SoundRequest.NEEDS_UNPLUGGING_BADLY)
    s2 = soundhandle.waveSound("say-beep.wav")
    s3 = soundhandle.voiceSound("Testing the new A P I")
    s4 = soundhandle.builtinSound(SoundRequest.NEEDS_UNPLUGGING_BADLY, 0.3)
    s5 = soundhandle.waveSound("say-beep.wav", 0.3)
    s6 = soundhandle.voiceSound("Testing the new A P I", 0.3)

    rospy.loginfo("New API start voice")
    s3.repeat()
    rospy.sleep(3)

    rospy.loginfo("New API start voice quiet")
    s6.play()
    rospy.sleep(3)

    rospy.loginfo("New API wave")
    s2.repeat()
    rospy.sleep(2)

    rospy.loginfo("New API wave quiet")
    s5.play()
    rospy.sleep(2)

    rospy.loginfo("New API builtin")
    s1.play()
    rospy.sleep(2)

    rospy.loginfo("New API builtin quiet")
    s4.play()
    rospy.sleep(2)

    rospy.loginfo("New API stop")
    s3.stop()

    
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException: pass


