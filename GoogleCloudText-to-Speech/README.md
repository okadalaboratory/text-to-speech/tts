# Google Cloud Text-to-Speech
日本語を含め複数の言語に対応し、機械学習を利用して自然な発音の音声を合成することができます。

有償なので使い過ぎには注意してください。

事前の準備が少し手間ですが、順を追って説明します。

## 簡単に試してみる
[Google Text-to-SpeechのWebサイト](https://cloud.google.com/text-to-speech/)から簡単に試すことができます。
![簡単に試してみよう](../images/スクリーンショット 2022-12-06 11.52.58.png)

## Google アカウントの作成
Googleアカウントが必要です。

## Google Cloud Platform の登録
Googlaアカウントにログインし、Googleクラウドのページにアクセスします。
![Googleクラウド](../images/スクリーンショット 2022-12-06 13.16.06.png)
「無料で使ってみる」ボタンをクリックしてください。

しばらくして、登録が完すると Google Cloud Platform のコンソール（管理画面）が表示されます。

## プロジェクトの作成
Cloud Text-to-Speech に新しいプロジェクトを作成します。

## Cloud Text-to-Speech API を有効化
左上のハンバーガー「≡」メニューから「APIとサービス」＞「ライブラリ」を選択します。

検索窓から「Text-to-Speech」を検索して、「Cloud Text-to-Speech API」を選び、「有効にする」ボタンをクリックします。

## Google Cloud SDK のインストール
Cloud Text-to-Speech API の認証情報を作成するのに Google Cloud SDK が必要になるので、下記の通りインストールします。

SDKをインストールしたいディレクトリに移動して、下記のコマンドを実行します。
```
$ cd ~
$ curl https://sdk.cloud.google.com | bash
```
インストールの途中でいくつか質問されますが、デフォルトのままで問題ありません。
Google Cloud SDKの設定が ~/.bashrcあるいは ~/.bash_profileに書き込まれるので、シェルの設定ファイルを読み直してください。
```
source ~/.bashrc
もしくは
source ~/.bash_profile
```
gcloudのヘルプが表示されればインストールは正常に終了しています。
```
$ gcloud help
```

## Google Cloud SDK の初期化
### Google Cloud SDK の認証
下記のコマンドでGoogleアカウントにログインした状態でGoogle Cloud SDKをアプリ認証します。
```
$ gcloud init
```
インストールの途中でいくつか質問されますが、デフォルトのままで問題ありません。
「Google Cloud SDK の認証が完了しました」が表示されたら、ターミナル（コマンドプロンプト）に戻ります。

### プロジェクトIDの選択
ターミナルに戻ったら、先ほど作成したプロジェクトのIDを選択します。
```
Pick cloud project to use:
[1] my-test-project-203409
[2] my-test-project-201123 
...
...
[n] Create a new project
Please enter numeric choice or text value (must exactly match list
item): 2
```
Google Cloud SDK の設定情報は ~/.config/gcloud/ 以下に保存されるので必要があれば修正してください。

## 認証情報の作成
「IAM と管理」＞「サービスアカウント」を選択します。

「＋サービスアカウントを作成」をクリックします。

適当なサービスアカウント名を入力し、「作成」をクリックします。

そのまま「続行」をクリックします。

「＋キーの作成」をクリックします。

キーのタイプ「JSON」を選択して「作成」をクリックします。

キーファイルがダウンロードされます。

ダウンロードしたキーファイルを使って認証情報を設定します。
```
$ gcloud auth activate-service-account --key-file=＜キーファイルのパス＞
```
環境変数「GOOGLE_APPLICATION_CREDENTIALS」にもキーファイルのパスを追加しておきます。

export GOOGLE_APPLICATION_CREDENTIALS=＜キーファイルのパス＞


ここまでで準備は完了です。サンプルプログラムを試してみましょう。



## 参考
[Google Cloud Platformへの登録](https://blog.apar.jp/web/9893/)
