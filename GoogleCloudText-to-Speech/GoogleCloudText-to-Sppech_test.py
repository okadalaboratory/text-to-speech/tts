#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.
import base64
import numpy as np
import urllib.request
import json
import subprocess as sp
from playsound import playsound

def get_token() -> str:
    """
    Google Text-To-Speechの認証した上で、gcloudをセットアップした状態で
    tokenを取得するために、gcloud auth print-access-tokenの結果を取得する
    """
    res = sp.run('gcloud auth print-access-token',
            shell=True, stdout=sp.PIPE, stderr=sp.PIPE,
            encoding='utf-8')
    print(res.stderr)
    return res.stdout.strip()

def makeRequestDict(txt: str) -> dict:
    """
    Google Text-To-Speechへリクエストのための情報を生成する
    SSMLには未対応

    Args:
        txt(in): 音声合成するテキスト

    Returns:
        音声合成するために必要な情報をdictで返却する
    """
    dat = {"audioConfig": {
        "audioEncoding": "LINEAR16",
        "pitch": 0,
        "speakingRate": 1
      },
      "voice": {
        "languageCode": "ja-JP",
        "name": "ja-JP-Standard-B"
      }
    }

    dat["input"] = {"text": txt}
    return dat

def output_wav(dat: dict, ofile: str) -> None:
    """
    Google Text-To-Speechへリクエストした結果を元に音声データにしてファイルに書き込む

    Args:
        dat(in):   リクエストした結果得られたJSON文字列をdictにしたもの
        ofile(in): 音声データを書き出すファイル名
    """
    b64str = dat["audioContent"]
    binary = base64.b64decode(b64str)
    dat = np.frombuffer(binary,dtype=np.uint8)
    with open(ofile,"wb") as f:
        f.write(dat)

def gtts(txt: str, ofile: str) -> None:

    dat = makeRequestDict(txt)
    req_data = json.dumps(dat).encode()

    url = 'https://texttospeech.googleapis.com/v1beta1/text:synthesize'
    token = get_token()
    req_header = {
            'Authorization': f"Bearer {token}",
            'Content-Type': 'application/json; charset=utf-8',
    }
    req = urllib.request.Request(url, data=req_data, method='POST', headers=req_header)

    try:
        with urllib.request.urlopen(req) as response:
            dat = response.read()
            body = json.loads(dat)
            output_wav(body, ofile)
            print("done..")
    except urllib.error.URLError as e:
        print("error happen...")
        print(e.reason)
        print(e)


if __name__ == "__main__":
    gtts("こんにちは、私の名前は太郎です。東京都出身です。", "/tmp/out.wav")
    playsound("/tmp/out.wav")
