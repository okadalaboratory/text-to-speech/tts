# VoiceText Web API
## Webに声を、感情を。
高品質な音声合成VoiceTextが、簡単に使えるWeb APIに。<br>
日本語のみで、英語には対応していません。<br>
インターネットに接続している必要があります。

## 試した環境
MacBook Air（M2、2022）,macOS Monterey 12.6<br>
Ubuntu20.04

## 利用登録
API無料利用登録ページ　https://cloud.voicetext.jp/webapi/api_keys/new　から登録すると、メールでAPIキーが届きます。

## 音声合成を試す
環境変数のVoiceTextWebAPIKeyにメールで送付されたAPIキーを設定します。APIキーの最後にコロン（：）を付けるのを忘れないでください。

```
$ export VoiceTextWebAPIkey=XXXXXXXXXXXX:
```

## 合成された音声をファイルに保存する
```
$ curl "https://api.voicetext.jp/v1/tts"    \
  -o "test.wav"  \
  -u $VoiceTextWebAPIkey \
  -d "text=こんにちは、今日はいい天気ですね"  \
  -d "speaker=hikari"
```

## 合成された音声をそのまま再生する
```
$ curl "https://api.voicetext.jp/v1/tts"    \
  -u $VoiceTextWebAPIkey \
  -d "text=こんにちは、今日はいい天気ですね"  \
  -d "speaker=hikari" | play -
```
Ubuntuの場合、音声再生ソフトウェアのplay をインストールして下さい。
```
$ sudo apt-get insttall sox
```

## 感情を指定して音声を合成する
感情を指定して音声を合成することもできます。
```
$ curl "https://api.voicetext.jp/v1/tts" \
     -u $VoiceTextWebAPIkey \
     -d "text=おはようございます" \
     -d "emotion=sadness" \
     -d "emotion_level=2" \
     -d "speaker=hikari" | play -
```
話す速度、声の高さ、感情の種類などいろいろなパラメーターがあります。

話者一覧:<br>
言語 　　　　話者名<br>
日本語　　　show(男性)<br>
日本語　　　haruka(女性)<br>
日本語　　　hikari(女性)<br>
日本語　　　takeru(男性)<br>
日本語　　　santa(サンタ)<br>
日本語　　　bear(凶暴なクマ)<br>

詳しくは API マニュアル を参照してください。

## VoiceText Web APIライブラリ
```
$ pip3 install pyVoiceText
```
### 音声合成した結果をファイル（out.wav）に保存する。
```
$ python3
>>> from pyVoiceText import VoiceText
>>> voice_text = VoiceText("YOUR_API_KEY")
>>> wav = voice_text.fetch("皆さんこんにちは、今日はいい天気ですね", "show", "out.wav")
```

### Pythonプログラムのサンプル
#### 音声再生Pythonライブラリのインストール
Ubuntu <br>
```
pip3 install playsound
```
macOS<br>
```
pip3 install playsound
pip3 install PyObjC
```
#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import requests
from requests.auth import HTTPBasicAuth
from pyVoiceText import VoiceText
from playsound import playsound

class VoiceText(object):
    VERSION = "v1"
    URL = "https://api.voicetext.jp/%s/tts" % VERSION

    def __init__(self, key):
        self.key = key

    def fetch(self, text, speaker, out=None, emotion=None,
              emotion_level=1, pitch=100, speed=100, volume=100):
        params = {
            "text": text,
            "speaker": speaker,
            "pitch": pitch,
            "speed": speed,
            "volume": volume
        }
        if emotion:
            params["emotion"] = emotion
            params["emotion_level"] = emotion_level

        wave = self._request(params)
        if out:
            self.save(wave, out)
            return True

        return wave

    def _request(self, params):
        auth = HTTPBasicAuth(self.key, "")
        resp = requests.post(self.URL, params=params, auth=auth)

        if resp.status_code == 200:
            return resp.content
        else:
            content = json.loads(resp.content)
            message = content["error"]["message"]
            raise Exception("%s: %s" % (resp.status_code, message))

    def save(self, wave, out):
        with open(out, "wb") as f:
            f.write(wave)

def main():
    voice_text = VoiceText("YOUR_API_KEY"")
    voice_text.fetch("私の名はHSRです ", "show", "/tmp/out.wav")
    playsound("/tmp/out.wav")

if __name__ == "__main__":
    main()
```
