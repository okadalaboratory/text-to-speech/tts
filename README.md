# テキストを音声に変換する　 Text-to-Speech
* オンライン |  オフライン

* 日本語 | 外国語

* Ubuntu | macOS | Windows

## 準備
Pythonプログラムから音声ファイルを再生するために playsoundをインストールしてください。<br>
Ubuntu <br>
```
pip3 install playsound
```
macOS<br>
```
pip3 install playsound
pip3 install PyObjC
```

## VoiceText Web API
品質:S（秀）<br>
オンライン<br>
日本語のみ<br>
MacBook Air（M2、2022）,macOS Monterey 12.6<br>
Ubuntu20.04<br>

## sound_play
品質：B（良）<br>
オフライン<br>
英語のみ<br>
ROSパッケージ<br>
音声合成の他、音ファイルの再生や内蔵音源の再生が便利。


## Google gTTS
品質：A<br>
オンライン<br>
日本語、英語<br>
MacBook Air（M2、2022）,macOS Monterey 12.6<br>
Ubuntu20.04<br>
音声合成のコアな部分は、Google翻訳音声合成APIに依存し、Google Cloud Text-to-Speechとは関係ない。

事前の設定もなく、無料で簡単に試せる

## Google Cloud Text-to-Speech
品質：S<br>
オンライン<br>
日本語、英語<br>
MacBook Air（M2、2022）,macOS Monterey 12.6<br>
Ubuntu20.04<br>
有償かつ事前の設定が面倒だが、性能は良い。












